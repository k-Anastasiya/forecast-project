@Smoke
Feature: Weather API Testing

  Background: I have a valid API key

  Scenario Outline: Positive Weather Test for City
    Given I request weather for city "<city>"
    Then the response status code should be 200
    And the response should contain valid weather data for city "<city>"

    Examples:
      | city     |
      | London   |
      | Paris    |
      | New York |
      | Tokyo    |