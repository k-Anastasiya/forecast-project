@Smoke
Feature: Weather API Testing

  Background: I have a valid API key

  Scenario Outline: Negative Weather Test with Error
    When I request weather for city with error type "<errorType>"
    Then the response status code should be 200
    And the response should contain an error type "<errorType>"

    Examples:
      | errorType          |
      | invalid_access_key |
      | missing_query      |
      | missing_access_key |
