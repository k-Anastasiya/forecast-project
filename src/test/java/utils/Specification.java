package utils;


import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Specification {
    private static final String BASE_URL = "http://api.weatherstack.com";

    public static RequestSpecification requestSpecification() {
        RequestSpecification requestSpecification = new RequestSpecBuilder()
               .addFilter(new AllureRestAssured())
                .setBaseUri(BASE_URL)
                .setContentType("application/json")
                .build();
        return requestSpecification;
    }

    public static ResponseSpecification responseSpecification() {
        ResponseSpecification responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .build();
        return responseSpecification;

    }

    public static void installSpec(ResponseSpecification responseSpecification) {
        RestAssured.responseSpecification = responseSpecification();
    }

    public static void installSpec(RequestSpecification requestSpecification) {
        RestAssured.requestSpecification = requestSpecification();
    }

    public static void installSpec(RequestSpecification requestSpecification, ResponseSpecification responseSpecification) {
        RestAssured.requestSpecification = requestSpecification();
        RestAssured.responseSpecification = responseSpecification();
    }

}

