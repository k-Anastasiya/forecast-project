package cucumberSteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import pojos.error.ResponseErrorPojo;

import static io.restassured.RestAssured.given;
import static utils.Specification.*;

@Slf4j
public class WeatherApiTest {
    private static Response response;
    private static final String API_KEY = "bd3efcf0780347c1e6ada6148b46eebb";

    @Step("I request weather for city {city}")
    @Given("I request weather for city {string}")
    public void RequestWeatherForCity(String city) {
        installSpec(requestSpecification(), responseSpecification());
        sendResponse(city);
        response.then().log().all();
    }

    @Step
    public static Response sendResponse(String city) {
        response = given()
                .queryParam("access_key", API_KEY)
                .queryParam("query", city)
                .when()
                .get("/current");
        return response;
    }

    @Step("The response status code should be 200")
    @Then("the response status code should be 200")
    public void theResponseStatusCodeShouldBe() {
        Assert.assertEquals(200, response.getStatusCode());
    }

    @Step("The response should contain valid weather data for city {city}")
    @And("the response should contain valid weather data for city {string}")
    public void theResponseShouldContainValidWeatherDataForCity(String city) {
        response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("ForecastTemplate.json"));
    }

    @Step("I request weather for city with error type {errorType}")
    @When("I request weather for city with error type {string}")
    public void iRequestWeatherForCityWithErrorType(String errorType) {
        installSpec(requestSpecification(), responseSpecification());
        sendResponseWithError(errorType);
        response.then().log().all();
    }

    @Step
    public static Response sendResponseWithError(String errorType) {
        RequestSpecification requestSpecification = given();
        switch (errorType) {
            case "missing_query":
                requestSpecification.queryParam("access_key", API_KEY);
                break;
            case "invalid_access_key":
                requestSpecification.queryParam("access_key", "invalid_key");
                requestSpecification.queryParam("query", "London");
                break;
            case "missing_access_key":
                requestSpecification.queryParam("query", "London");
                break;
            default:
                throw new IllegalArgumentException("Unknown error type: " + errorType);
        }
        response = requestSpecification.get("/current");
        return response;
    }

    @Step("The response should contain an error type {errorType}")
    @And("the response should contain an error type {string}")
    public void theResponseShouldContainAnErrorType(String errorType) {
        String expectedError = chooseExpectedError(errorType);
        Allure.step("Expected Error: {}", Status.valueOf(expectedError));
        String actualError = response.then().extract().as(ResponseErrorPojo.class).getError().getInfo();
        Allure.step("Actual Error: {}", Status.valueOf(actualError));
        response.then().assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("ForecastTemplate.json"));
        Assert.assertEquals(expectedError, actualError, "Error mismatch -");
    }

    @Step
    public static String chooseExpectedError(String error) {
        switch (error) {
            case "invalid_access_key":
                return "You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com]";
            case "missing_query":
                return "Please specify a valid location identifier using the query parameter.";
            case "missing_access_key":
                return "You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY]";
            default:
                throw new IllegalArgumentException("Unknown error: " + error);
        }
    }
}