package pojos.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseErrorPojo {

    private boolean success;
    private ErrorPojo error;

}