package pojos.current;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastPojo {

	@JsonProperty("request")
	private RequestPojo request;

	@JsonProperty("current")
	private CurrentPojo current;

	@JsonProperty("location")
	private LocationPojo location;
}