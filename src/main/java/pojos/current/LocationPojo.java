package pojos.current;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import utils.DateDeserializer;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationPojo {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    @JsonDeserialize(using = DateDeserializer.class)
    private LocalDateTime localtime;

    @JsonProperty("utc_offset")
    private String utcOffset;

    private String country;

    @JsonProperty("localtime_epoch")
    private int localtimeEpoch;

    private String name;

    @JsonProperty("timezone_id")
    private String timezoneId;

    private String lon;

    private String region;

}