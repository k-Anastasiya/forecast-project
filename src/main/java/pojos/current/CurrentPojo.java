package pojos.current;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentPojo {

	@JsonProperty("weather_descriptions")
	private List<String> weatherDescriptions;

	@JsonProperty("observation_time")
	private String observationTime;

	@JsonProperty("wind_degree")
	private int windDegree;

private int visibility;

	@JsonProperty("weather_icons")
	private List<String> weatherIcons;

	private int feelslike;

	@JsonProperty("is_day")
	private String isDay;

	@JsonProperty("wind_dir")
	private String windDir;

	private int pressure;

	private int cloudcover;

	private int precip;

	@JsonProperty("uv_index")
	private int uvIndex;

	private int temperature;

	private int humidity;

	@JsonProperty("wind_speed")
	private int windSpeed;

	@JsonProperty("weather_code")
	private int weatherCode;

}