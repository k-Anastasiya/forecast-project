
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/scenario",
        glue = "cucumberSteps",
        tags="@Smoke",
        plugin = {"pretty", "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"})
public class Runner extends AbstractTestNGCucumberTests {

}
